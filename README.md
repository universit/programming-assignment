# Programming assignment Universit

You can create this programming assignment in any language that you prefer, as long as you expect to be able to achieve the desired functionality within 2-6 hours. The assignment is very flexible on purpose and you are invited to come up with a resonable input/output format.

## Functionality

The goal is to create an API that receives a set of twitter usernames, typically 5-10, and will lookup the latest tweets on Twitter for those users. After the tweets are retrieved from Twitter, the 25 most recent tweets of all users combined should be returned, in global chronological order.

A UI is not needed, we expect some sort of API, that can be consumed over a network.

## Guidelines

These are some things to think of when building your solution:

- You are not allowed to use the Twitter Search API, any other endpoints are fine
- Input validation
- Performance: your solution should ideally scale so it can handle quite a few usernames and still give back a
response within a reasonable timeframe. It is up to you to decide what is "reasonable"
- Documentation: we expect some form of documentation on how to run your service and how to consume your API
- It is completely fine to use existing libraries, as long as you can explain your choices

## Twitter API access

If you have a Twitter account of your own, it's easy to get a set of API keys at https://apps.twitter.com
If you don't have a Twitter account and do not feel like creating one, you can contact us for a set of
API keys.

## Delivery

We prefer it if you send us a link to the source code of your programming assignment in a version control system
of your choice. Please make the project private, not public, and give us access. If this poses problems, you can
also email us the source code and any documentation as a zip file.

Our bitbucket usernames are _bram_koot_, _dandydev_ and _mkrcah_.
